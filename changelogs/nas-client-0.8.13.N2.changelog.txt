*****************************************************************************
Changes in Nas 0.8.13.N2 relative to the original distribution of 
the creator of NAS, Nas 0.8.13(N1).
*****************************************************************************

Numbering of version using dot instead of parenthesis (as in the name of this file)

Cleaned spacing and position on some files, to be the closest as in Nxt 0.8.13

### Added files:
+ _List4GenesisBlock_, from <https://bitbucket.org/nascointeam/nascoinproject>. 
  Its information is used in _src/java/nxt/Genesis.java_, but it is an important file in the history of Nas
+ _.gitignore_, taken from Nxt 0.8.13, to help version management with Git
+ _MIT-license.txt_, taken from Nxt 0.8.13 and adapted to Nas; a license is a must

### Modified files:
+ _conf/.gitignore_: changed `/nxt.properties` into `/nas.properties`
+ _conf/nas-default.properties_: corrected yet more `nxt*` for `nas*`
+ _src/java/nxt/Nxt.java_: final logMessage edited
+ _src/java/nxt/TransactionType.java_: corrected harcoded `30` in line 561
+ _src/java/nxt/VerifyTrace.java_: corrected `"nxt.trace"` for `"nas.trace"`, line 42
+ _src/java/nxt/http/GetAllOpenOrders.java_: added `orderData.put("quantity", order.getQuantity());`
+ _src/java/nxt/http/GetUnconfirmedTransactionsIds.java_: removed 4 imports
+ files with corrected month in origin of time at script function formatTimestamp:
    - _html/nrs/myTransaction.html_
    - _html/nrs/orphanedBlocks.html_
    - _html/nrs/recentBlocks.html_
    - _html/nrs/unconfirmedTransactions.html_

### Files with only spacing corrections:
+ _readme.txt_: also, Markdown syntax used
+ _src/java/nxt/Account.java_
+ _src/java/nxt/Asset.java_
+ _src/java/nxt/Attachment.java_
+ _src/java/nxt/BlockDb.java_
+ _src/java/nxt/BlockImpl.java_
+ _src/java/nxt/DbVersion.java_
+ _src/java/nxt/DebugTrace.java_
+ _src/java/nxt/TransactionImpl.java_
+ _src/java/nxt/http/BroadcastTransaction.java_
+ _src/java/nxt/http/JSONResponses.java_
+ _src/java/nxt/http/TransferAsset.java_
