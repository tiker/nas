package nxt.util;

public interface Listener<T> {

    void notify(T t);

}